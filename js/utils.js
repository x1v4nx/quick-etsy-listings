(function() {
  "use strict";

  // Define 'etsy.utils' namespace
  window.etsy.utils = {};

  // DOM module
  etsy.utils.dom = (function () {

    return {
      addEventListener: (window.document.addEventListener
        ? function(element, type, fn) { element.addEventListener(type, fn, false); }
        : function(element, type, fn) { element.attachEvent('on' + type, fn); }),
      removeEventListener: (window.document.removeEventListener
        ? function(element, type, fn) { element.removeEventListener(type, fn, false); }
        : function(element, type, fn) { element.detachEvent('on' + type, fn); })
    };

  })();

  // Server module
  etsy.utils.server = (function () {

    etsy.callbacks = {
      counter: 0
    };

    etsy.activeRequests = 0;

    function makeAjaxCall(options) {
      var callbacks = etsy.callbacks,
          callbackId = '_' + (callbacks.counter++),
          currentCallback,
          isCrossDomain,
          i, l;

      // TODO: replace URL placeholders here!
      if (options.urlKey) {
        options.url = etsy.dataSource.getFullUrl(options.urlKey, 'etsy.callbacks.' + callbackId);
      }

      if (!options.url) {
        throw new Error('The URL must be provided.');
      }

      isCrossDomain = etsy.utils.urls.isCrossDomain(options.url);

      if (!etsy.config.getEntry('corsCompatibility') && isCrossDomain) {
        etsy.activeRequests++;

        currentCallback = callbacks[callbackId] = function(jsonObj) {
          currentCallback.data = jsonObj;
          currentCallback.invoked = true;
        };

        if (options.data instanceof Array) {
          for (i = 0, l = options.data.length; i < l; i++) {
            options.url = etsy.utils.urls.addParameter(options.url, options.data[i]['key'], options.data[i]['value']);
          }
        } else if (options.data instanceof Object) {
          for (i in options.data) {
            options.url = etsy.utils.urls.addParameter(options.url, i, options.data[i]);
          }
        }

        jsonpRequest(options.url, currentCallback, {
          error: options.error || null,
          success: options.success || null,
          complete: function () {
            if (typeof(options.complete) === 'function') {
              options.complete.apply(null, arguments);
            }
            delete callbacks[callbackId];
            etsy.activeRequests--;
          }
        });
      } else {
        // TODO: Replace with native AJAX calls
        $.ajax($.extend({ type: 'POST', async: true, crossDomain: isCrossDomain, cache: false }, options, {
          dataType: options.isHtml ? 'html' : 'json',
          contentType: (options.isHtml ? 'application/x-www-form-urlencoded' : 'application/json') + '; charset=utf-8'
        }));
      }
    };

    function jsonpRequest(url, jsonpCallback, callbacks) {
      var script = document.createElement('script'),
          listener;

      if (!callbacks) {
        script = null;
        throw new Error('You must provide callbacks.');
      }

      script.type = 'text/javascript';
      script.src = url;
      script.async = true;

      listener = function(event) {
        var completedRequest,
            status = -1,
            text = 'unknown';

        etsy.utils.dom.removeEventListener(script, 'load', listener);
        etsy.utils.dom.removeEventListener(script, 'error', listener);

        document.body.removeChild(script);

        script = null;

        if (event) {
          if (event.type === 'load' && (!jsonpCallback.invoked || !jsonpCallback.data || !jsonpCallback.data.ok)) {
            event = { type: 'error' };
          }
          text = event.type;
          status = event.type === 'error' ? 404 : 200;

          if (event.type === 'error') {
            if (typeof(callbacks.error) === 'function') {
              callbacks.error(status, text);
              return;
            } else {
              throw new Error('The request has failed. ' + (event.message || ''));
            }
          }
        }

        if (typeof(callbacks.success) === 'function') {
          completedRequest = completeRequest(callbacks.success, jsonpCallback.data, '', status, text);
        }

        if (typeof(callbacks.complete) === 'function') {
          if (!completedRequest) {
            completedRequest = completeRequest(callbacks.complete, jsonpCallback.data, '', status, text);
          } else {
            callbacks.complete(completedRequest.status, completedRequest.response, completedRequest.headersString, completedRequest.statusText);
          }
        }

        if (!completedRequest)
          throw new Error('The request could not be handled.');
      };

      etsy.utils.dom.addEventListener(script, 'load', listener);
      etsy.utils.dom.addEventListener(script, 'error', listener);

      document.body.appendChild(script);
    };

    function completeRequest(callback, response, headersString, status, statusText) {
      if (status === 0) {
        status = response ? 200 : etsy.utils.urls.resolve(url).protocol == 'file' ? 404 : 0;
      }

      // normalize IE bug (http://bugs.jquery.com/ticket/1450)
      status = status === 1223 ? 204 : status;
      statusText = statusText || '';

      callback(response, headersString, status, statusText);

      return {
        status: status,
        response: response,
        headersString: headersString,
        statusText: statusText
      };
    };

    return {
      makeAjaxCall: makeAjaxCall
    };

  })();

  // URLs module
  etsy.utils.urls = (function() {

    function resolve(url) {
      var msie,
          userAgent = (navigator.userAgent || '').toLowerCase(),
          urlParsingNode = document.createElement('a');

      if (userAgent) {
        msie = parseInt((/msie (\d+)/.exec(userAgent) || [])[1], 10);
        if (isNaN(msie)) {
          msie = parseInt((/trident\/.*; rv:(\d+)/.exec(userAgent) || [])[1], 10);
        }

        if (msie) {
          urlParsingNode.setAttribute('href', url);
          url = urlParsingNode.href;
        }
      }

      urlParsingNode.setAttribute('href', url);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) !== '/' ? '/' : '') + urlParsingNode.pathname
      };
    };

    function addParameter(url, parameter, value) {
      if (url) {
        if (parameter.search(/^:/) > -1) {
          // Replace URL placeholder with value
          url = url.replace(parameter, value);
        } else {
          // Add parameter to URL query string
          url += (url.search(/\?/) > -1 ? '&' : '?') + parameter + '=' + value;
        }
      }

      return url;
    };

    function isCrossDomain(url) {
      var currentUrl = resolve(''),
          resolvedUrl = resolve(url);

      // TODO: check if more conditions are required, such as port equality
      return resolvedUrl.protocol !== 'file' && (resolvedUrl.host && resolvedUrl.host !== 'localhost' && resolvedUrl.host !== '127.0.0.1' && currentUrl.host !== resolvedUrl.host);
    }

    return {
      resolve: resolve,
      addParameter: addParameter,
      isCrossDomain: isCrossDomain
    };

  })();

  // Dates module
  etsy.utils.dates = (function() {

    function fromTimestamp(timestamp) {
      return new Date(timestamp * 1000);
    };

    function toTimestamp(date) {
      return Math.round(date.getTime() / 1000);
    };

    // TODO: use the given format
    function formatDate(date, format) {
      format = format || '';

      if (typeof(date) !== 'object') {
        date = fromTimestamp(date);
      }

      return date;
    };

    return {
      fromTimestamp: fromTimestamp,
      toTimestamp: toTimestamp,
      formatDate: formatDate
    };

  })();

  // Storage module
  etsy.utils.storage = (function() {

    var configKey = 'userConfig';

    function getItem(key) {
      return JSON.parse(localStorage.getItem(key));
    };

    function setItem(key, data) {
      return localStorage.setItem(key, typeof (data) === 'string' ? data : JSON.stringify(data));
    };

    function getConfig() {
      return getItem(configKey);
    };

    function setConfig(data) {
      return setItem(configKey, data);
    };

    return {
      getItem: getItem,
      setItem: setItem,
      getConfig: getConfig,
      setConfig: setConfig
    };

  })();

})();
