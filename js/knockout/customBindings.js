(function () {

  ko.bindingHandlers.i18n = {
    init: ko.bindingHandlers.text.init,
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
      var i18nValueAccessor = function () {
        var value = ko.unwrap(valueAccessor());

        return i18n.t(typeof(value) === 'object' ? ko.unwrap(value.key) : value, $.extend({ lng: viewModel.locale() }, ko.unwrap(value.opts) || {}));
      };

      ko.bindingHandlers.text.update(element, i18nValueAccessor);
    }
  };
  ko.virtualElements.allowedBindings.i18n = true;

})();
