$(document)
  .ready(function() {

    $('.ui.config.sidebar')
      .sidebar('attach events', '.config.button')
    ;

    $('.ui.confirm.modal')
      .modal('setting', {
        closable: false,
        onDeny: function(){
          
        },
        onApprove: function() {
          
        }
      })
    ;

    $('.ui.grid .table .trash.icon')
      .click(function(ev) {
        $('.ui.confirm.modal')
          .modal('show')
        ;
        ev.stopPropagation();
      })
    ;

    $('.ui.grid .table tbody tr')
      .click(function(ev) {
        $('.ui.details.modal')
          .modal('show')
        ;
      })
    ;

    $('.popup[title]')
      .popup({
        className: {
          popup: 'ignored ui popup'
        }
      })
    ;

    $('.ui.dropdown')
      .dropdown()
    ;

    $('.ui.checkbox')
      .checkbox()
    ;

  })
;
