(function() {
  "use strict";

  // Define 'etsy' namespace
  window.etsy = {};

  // App Start
  etsy.start = (function() {

    var initialized = false;

    function startApp(options, callback) {
      options = options || {};

      if (initialized) {
        throw new Error('The application has already been started.');
      }

      if (!etsy.utils) {
        throw new Error('The application utils must be loaded first.');
      }

      // Create Application Context
      etsy.appContext = (function() {

        function Config(env, userConfig) {
          var defaultConfig = {
                apiKey: '1bt99tastcfvz0btxxewplgp',
                corsCompatibility: false,
                infiniteScroll: false,
                resources: ['User', 'Images', 'MainImage']
              };

          this.setAll($.extend(defaultConfig, userConfig || {}, !options.skipStorage ? (etsy.utils.storage.getConfig() || {}) : {}));
        };

        Config.prototype = {
          get: function(entry) {
            if (!(entry in this.entries)) {
              throw new Error('The given config entry does not exist.');
            }

            return this.entries[entry];
          },
          set: function(entry, value) {
            this.entries[entry] = value;

            etsy.utils.storage.setConfig(this.entries);
          },
          setAll: function(values) {
            this.entries = values;

            etsy.utils.storage.setConfig(this.entries);
          }
        };

        function DataSource(env) {
          var baseUrl, urls;

          switch (env) {
            case 'prod':
              baseUrl = 'https://openapi.etsy.com/v2/';

              urls = {
                listings: baseUrl + 'listings/active',
                details: baseUrl + 'listings/:listing_id',
                categories: baseUrl + 'taxonomy/categories'
              };
              break;

            case 'dev':
              urls = {
                listings: 'js/jsonMocks/listings',
                details: 'js/jsonMocks/details',
                categories: 'js/jsonMocks/categories'
              };
              break;

            case 'test':
              urls = {
                listings: 'tests/data/listings',
                details: 'tests/data/details',
                categories: 'tests/data/categories'
              };
              break;

            default:
              throw new Error('The given environment does not exist.');
          }

          this.urls = urls;
        };

        DataSource.prototype = {
          getRaw: function(key) {
            if (!this.urls[key]) {
              throw new Error('The given datasource key does not exist.');
            }

            return this.urls[key];
          },
          getFormatted: function(key, callbackName) {
            var url = this.getRaw(key),
                resources = etsy.config.getEntry('resources'),
                resourcesParamValue,
                apiKeyString,
                i, l;

            if (!etsy.config.getEntry('corsCompatibility') && etsy.utils.urls.isCrossDomain(url)) {
              url += '.js?callback=JSON_CALLBACK';
              if (typeof(callbackName) === 'string') {
                url = url.replace('JSON_CALLBACK', callbackName);
              }
            }

            apiKeyString = etsy.config.getEntry('apiKey');
            if (apiKeyString) {
              url = etsy.utils.urls.addParameter(url, 'api_key', apiKeyString);
            }

            if (resources.length) {
              resourcesParamValue = resources.join(',');
              url = etsy.utils.urls.addParameter(url, 'includes', resourcesParamValue);
            }

            return url;
          }
        };

        function AppContext(currentEnvironment) {
          var config = new Config(currentEnvironment, options.config),
              dataSource = new DataSource(currentEnvironment);

          this.getConfig = function (urlKey) {
            return config;
          };

          this.getDataSource = function (urlKey) {
            return dataSource;
          };
        };

        return new AppContext(options.environment || 'prod');

      })();

      // User Config (Shortcut for 'etsy.appContext.getConfig' method)
      etsy.config = (function() {

        var currentConfig = etsy.appContext.getConfig();

        return {
          getEntry: function (entry) {
            return currentConfig.get(entry);
          },
          setEntry: function (entry, value) {
            return currentConfig.set(entry, value);
          }
        };

      })();

      // Data Source (Shortcut for 'etsy.appContext.getDataSource' method)
      etsy.dataSource = (function() {

        var currentDataSource = etsy.appContext.getDataSource();

        return {
          getBaseUrl: function (urlKey) {
            return currentDataSource.getRaw(urlKey);
          },
          getFullUrl: function (urlKey, callbackName) {
            return currentDataSource.getFormatted(urlKey, callbackName);
          }
        };

      })();

      // Initialize translations
      i18n.init({ lng: options.language || 'en', fallbackLng: 'en' }, callback);

      initialized = true;
    };

    return startApp;

  })();

  // Base Feature Models
  etsy.base = (function() {

    function Grid() {
      this.collection = ko.observableArray();

      this.pager = ko.observable();
    };

    function BasePager() {
      this.page;
    };

    BasePager.prototype = new ko.subscribable();

    function StandardPager() {
      var pager = this,
        page,
        amountPages = ko.observable(1),
        currentPage = ko.observable(1),
        getPageData = ko.observable(false);
   
      currentPage.subscribe(function (newPage) {
        if(getPageData())
            this.notifySubscribers(newPage, 'pageChanged');
      }, this);

      this.amountPages = ko.computed(function () {
        return amountPages();
      });

      this.currentPage = ko.computed({
        read: function () {
          return currentPage();
        },
        write: function (newPage) {
          getPageData(false);
          page = parseInt(Math.min(newPage, amountPages()), 10);

          if (page > 0 && parseInt(newPage, 10) <= amountPages() ) {
            getPageData(true);
            currentPage(page);  
          } else {
            currentPage.valueHasMutated();
          }

          if (newPage > page) {
            // Notify to update the value on the HTML bind
            currentPage.valueHasMutated();
          }
        },
        owner: this,
        deferEvaluation: true
      });

      this.prev = function () {
        page = currentPage();

        if (page > 1) {
          pager.currentPage(--page);
        }
      };

      this.next = function () {
        page = currentPage();

        if (page < amountPages()) {
          pager.currentPage(++page);
        }
      };

      this.first = function () {
        pager.currentPage(1);
      };

      this.last = function () {
        pager.currentPage(amountPages());
      };

      this.updateAmountPages = function (newAmount) {
        amountPages(!newAmount || isNaN(newAmount) ? 1 : parseInt(newAmount, 10));
        if (pager.currentPage() > amountPages())
          pager.last();
      };
    };

    StandardPager.prototype = new BasePager();

    function InfiniteScrollPager() {
      
    };

    InfiniteScrollPager.prototype = new BasePager();

    return {
      Grid: Grid,
      StandardPager: StandardPager,
      InfiniteScrollPager: InfiniteScrollPager
    };

  })();

  // Entity Models
  etsy.entities = (function() {

    function BaseEntity() {
      this.mapDataToObject = function () {
        throw new Error('Each entity model must implement this method.');
      };
    };

    function User(userData) {
      this.id;
      this.username;
      this.feedbackInfo = {
        count: 0,
        score: null
      };

      if (userData) {
        this.mapDataToObject(userData);
      }
    };

    User.prototype = new BaseEntity();
    User.prototype.mapDataToObject = function (data) {
      this.id = data.user_id;
      this.username = data.login_name;
      this.feedbackInfo.count = data.feedback_info.count;
      this.feedbackInfo.score = data.feedback_info.score;
    };

    function ListingCategory(listingCategoryData) {
      this.id;
      this.path;
      this.idPath;

      if (listingCategoryData) {
        this.mapDataToObject(listingCategoryData);
      }
    };

    ListingCategory.prototype = new BaseEntity();
    ListingCategory.prototype.mapDataToObject = function (data) {
      this.id = data.category_id;
      this.path = data.category_path;
      this.idPath = data.category_path_id;
    };

    function Category(categoryData) {
      this.id;
      this.name;
      this.shortDescription;
      this.longDescription;
      this.amountChildren;

      if (categoryData) {
        this.mapDataToObject(categoryData);
      }
    };

    Category.prototype = new BaseEntity();
    Category.prototype.mapDataToObject = function (data) {
      this.id = data.category_id;
      this.name = data.category_name;
      this.shortDescription = data.short_name;
      this.longDescription = data.long_name;
      this.amountChildren = data.num_children;
    };

    function Listing(listingData) {
      this.user;
      this.category;
      this.images;
      this.mainImage;

      this.id;
      this.userId;
      this.title;
      this.description;
      this.createdAt;
      this.endingAt;
      this.price;
      this.currencyCode;
      this.quantity;
      this.state;
      this.language;
//      this.statedAt;
//      this.originalCreatedAt;
//      this.lastModifiedAt;
//      this.shopSectionId;
//      this.shippingTemplateId;
      this.tags;
      this.materials;
      this.featuredRank;
      this.url;
      this.views;
      this.numFavorers;
      this.processingMin;
      this.processingMax;
      this.whoMade;
      this.whenMade;
      this.recipient;
      this.occasion;
      this.style;
      this.isSupply;
      this.isDigital;
      this.isCustomizable;
      this.fileData;
      this.hasVariations;
      this.nonTaxable;
      this.usedManufacturer;

      this.isActive = ko.observable(true);

      this.isFavourite = ko.observable(false);
      this.favouriteTranslation = ko.computed({
        read: function () {
          return i18n.t('grid.' + (this.isFavourite() ? 'unmarkAsFavourite' : 'markAsFavourite'));
        },
        owner: this,
        deferEvaluation: true
      });

      if (listingData) {
        this.mapDataToObject(listingData);
      }
    };

    Listing.prototype = new BaseEntity();
    Listing.prototype.mapDataToObject = function (data) {
      this.user = new User(data.User);
      this.category = new ListingCategory({
        category_id: data.category_id,
        category_path: data.category_path,
        category_path_ids: data.category_path_ids
      });
      this.images = data.Images;
      this.mainImage = data.MainImage;

      this.id = data.listing_id;
      this.userId = data.user_id;
      this.title = data.title;
      this.description = data.description;
      this.createdAt = data.creation_tsz;
      this.endingAt = data.ending_tsz;
      this.price = data.price;
      this.currencyCode = data.currency_code;
      this.quantity = data.quantity;
      this.state = data.state;
      this.language = data.language;
//      this.statedAt = data.state_tsz;
//      this.originalCreatedAt = data.original_creation_tsz;
//      this.lastModifiedAt = data.last_modified_tsz;
//      this.shopSectionId = data.shop_section_id;
//      this.shippingTemplateId = data.shipping_template_id;
      this.tags = data.tags;
      this.materials = data.materials;
      this.featuredRank = data.featured_rank;
      this.url = data.url;
      this.views = data.views;
      this.numFavorers = data.num_favorers;
      this.processingMin = data.processing_min;
      this.processingMax = data.processing_max;
      this.whoMade = data.who_made;
      this.whenMade = data.when_made;
      this.recipient = data.recipient;
      this.occasion = data.occasion;
      this.style = data.style;
      this.isSupply = data.is_supply;
      this.isDigital = data.is_digital;
      this.isCustomizable = data.is_customizable;
      this.fileData = data.file_data;
      this.hasVariations = data.has_variations;
      this.nonTaxable = data.non_taxable;
      this.usedManufacturer = data.used_manufacturer;

      this.isActive(data.isActive || true);
      this.isFavourite = ko.observable(data.isFavourite || false);
    };

    return {
      Category: Category,
      Listing: Listing
    };

  })();

})();
